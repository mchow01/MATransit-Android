#Overview
Finds the closest MBTA subway (Red Line, Orange Line, Blue Line) or
Commuter Rail train station within 5 miles given a user's location;
provides real-time train schedule for each station.

#Download on Google Play
https://play.google.com/store/apps/details?id=edu.tufts.cs.mchow.matransit
