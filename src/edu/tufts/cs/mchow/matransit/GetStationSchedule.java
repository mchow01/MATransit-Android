package edu.tufts.cs.mchow.matransit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import com.crashlytics.android.Crashlytics;
import android.os.AsyncTask;

public class GetStationSchedule extends AsyncTask<String, Integer, JSONArray> {
	// stop_name is required parameter
	private static final String STATION_SCHEDULE_API_URL = "http://mbtamap.herokuapp.com/mapper/station_schedule.json?stop_name=";

	@Override
	protected JSONArray doInBackground(String... arg0) {
		try {
			// http://stackoverflow.com/questions/2197993/escaping-in-a-url
			URL api = new URL(STATION_SCHEDULE_API_URL + URLEncoder.encode(arg0[0], "utf8"));
			HttpURLConnection conn = (HttpURLConnection)api.openConnection();

			// See http://stackoverflow.com/questions/2492076/android-reading-from-an-input-stream-efficiently
			InputStream is = conn.getInputStream();
			BufferedReader r = new BufferedReader(new InputStreamReader(is));
			StringBuilder total = new StringBuilder();
			String line;
			while ((line = r.readLine()) != null) {
			    total.append(line);
			}
			r.close();
			is.close();
			return new JSONArray(total.toString());
		}
		catch (MalformedURLException e) {
			Crashlytics.logException(e);
			return null;
		}
		catch (IOException e) {
			Crashlytics.logException(e);
			return null;
		}
		catch (JSONException e) {
			Crashlytics.logException(e);
			return null;
		}
	}
	
	/*@Override
	protected void onPostExecute(JSONArray schedule) {
    }*/
}
