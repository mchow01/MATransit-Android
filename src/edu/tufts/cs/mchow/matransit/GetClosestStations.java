package edu.tufts.cs.mchow.matransit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.crashlytics.android.Crashlytics;
import android.graphics.Color;
import android.os.AsyncTask;

public class GetClosestStations extends AsyncTask<LatLng, Integer, JSONArray> {
	// lat and lon are the required parameters
	private static final String CLOSEST_STATIONS_API_URL = "http://mbtamap.herokuapp.com/mapper/find_closest_stations.json?";

	public GetClosestStations() {
		super();
	}
	
	@Override
	protected JSONArray doInBackground(LatLng... arg0) {
		try {
			URL api = new URL(CLOSEST_STATIONS_API_URL + "lat="
					+ arg0[0].latitude + "&lon=" + arg0[0].longitude);
			HttpURLConnection conn = (HttpURLConnection)api.openConnection();

			// See http://stackoverflow.com/questions/2492076/android-reading-from-an-input-stream-efficiently
			InputStream is = conn.getInputStream();
			BufferedReader r = new BufferedReader(new InputStreamReader(is));
			StringBuilder total = new StringBuilder();
			String line;
			while ((line = r.readLine()) != null) {
			    total.append(line);
			}
			r.close();
			is.close();
			return new JSONArray(total.toString());
		}
		catch (MalformedURLException e) {
			Crashlytics.logException(e);
			return null;
		}
		catch (IOException e) {
			Crashlytics.logException(e);
			return null;
		}
		catch (JSONException e) {
			Crashlytics.logException(e);
			return null;
		}
	}
	
	@Override
	protected void onPostExecute(JSONArray closestStations) {
		// Display closest train station
		if (closestStations.length() > 0) {
			try {
				JSONObject closest = closestStations.getJSONObject(0).getJSONObject("station");
				String distance = closest.getString("distance");
				if (Double.parseDouble(distance) <= 1.0f) {
					MainActivity.mapFeedback.setText(MainActivity.mapFeedback.getText() +
						"\n\nThe closest station to you is " + closest.getString("stop_name")
						+ " which is " +  distance + " mile away from you. Click on marker on the map to see station's real-time train schedule.");
				}
				else {
					MainActivity.mapFeedback.setText(MainActivity.mapFeedback.getText() +
						"\n\nThe closest station to you is " + closest.getString("stop_name")
						+ " which is " +  distance + " miles away from you. Click on marker on the map to see station's real-time train schedule.");						
				}
				
				// Add polyline between you and the closest station
				PolylineOptions closestLine = new PolylineOptions()
					.add(new LatLng(Double.parseDouble(closest.getString("stop_lat")), Double.parseDouble(closest.getString("stop_lon"))))
					.add(new LatLng(MainActivity.me.myLat, MainActivity.me.myLng));
				Polyline polyline = MainActivity.map.addPolyline(closestLine);
				polyline.setColor(Color.GRAY);
				
				// Display markers on map
				for (int i = 0; i < closestStations.length(); i++) {
					JSONObject station = closestStations.getJSONObject(i).getJSONObject("station");
					distance = station.getString("distance");
					String snippet = "";
					if (Double.parseDouble(distance) <= 1.0f) {
						snippet = distance + " mile away from you";
					}
					else {
						snippet = distance + " miles away from you";
					}
					
					// RTFM: https://developers.google.com/maps/documentation/android/marker
					MainActivity.map.addMarker(new MarkerOptions()
						.position(new LatLng(Double.parseDouble(station.getString("stop_lat")),Double.parseDouble(station.getString("stop_lon"))))
						.title(station.getString("stop_name"))
						.snippet(snippet)
						.icon(BitmapDescriptorFactory.fromResource(R.drawable.t_marker))
						.draggable(false)
						);
				}
			}
			catch (JSONException e) {
				Crashlytics.logException(e);
			}
		}
		else {
			MainActivity.mapFeedback.setText(MainActivity.mapFeedback.getText() +
					"\n\nThere is no station within 5 miles of where you are.");
		}
		MainActivity.progress.dismiss();
    }
}
