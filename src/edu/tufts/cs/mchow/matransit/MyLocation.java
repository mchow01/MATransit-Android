package edu.tufts.cs.mchow.matransit;

import com.crashlytics.android.Crashlytics;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

public class MyLocation implements LocationListener {
	protected MainActivity bt;
	protected String myLocStr;
	protected boolean found;
	protected double myLat;
	protected double myLng;
	private Geocoder gc;
	
	public MyLocation (MainActivity bt) {
		this.bt = bt;
		found = false;
		myLat = 0;
		myLng = 0;
		
		// Set up geocoder
		gc = new Geocoder(bt.getApplicationContext(), Locale.getDefault());
	}
	
	@Override
	public void onLocationChanged(Location location) {
		found = false;
		
		myLat = location.getLatitude();
		myLng = location.getLongitude();
		myLocStr = "";
		
		// Geocoder
		try {
			// From http://mobiforge.com/developing/story/using-google-maps-android
			List <Address> addresses = gc.getFromLocation(myLat, myLng, 1);
			if (addresses.size() > 0) {
				int i = 0;
				for (i = 0; i < addresses.get(0).getMaxAddressLineIndex() - 1; i++) {
					myLocStr += addresses.get(0).getAddressLine(i) + ", ";
				}
				myLocStr += addresses.get(0).getAddressLine(i);
								
				// If we reached this point, then we're good
				found = true;
				
				// Redraw the map after getting location string
				bt.redrawLocation();				
			}
		}
		catch (IOException e) {
			myLocStr = bt.getString(R.string.unknown_location);
			Crashlytics.logException(e);
		}
		catch (Exception e) {
			myLocStr = bt.getString(R.string.unknown_location);
			Crashlytics.logException(e);
		}
	}

	@Override
	public void onProviderDisabled(String provider) {}

	@Override
	public void onProviderEnabled(String provider) {}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {}
}
