package edu.tufts.cs.mchow.matransit;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

public class SchedulePopupWindowAdapter implements InfoWindowAdapter {
	private LayoutInflater inflater;
	private Context context;
	
	public SchedulePopupWindowAdapter(LayoutInflater inflater, Context context) {
		this.inflater = inflater;
		this.context = context;
	}
	
	@Override
	public View getInfoContents(Marker marker) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		// Get station schedule asynchronously
		GetStationSchedule task = new GetStationSchedule();
		task.execute(marker.getTitle());
		
		// Using view group so another view can be programmatically created and appended
		// Source: http://stackoverflow.com/questions/3477581/android-add-a-view-to-a-specific-layout-from-code
		ViewGroup popupView = (ViewGroup)inflater.inflate(R.layout.schedule_popup, null);
		
		// Render stop name and distance away --like default info window
		TextView stopNameTV = (TextView) popupView.findViewById(R.id.stop_name);
		stopNameTV.setText(marker.getTitle());
		TextView distanceAwayTV = (TextView) popupView.findViewById(R.id.distance_away);
		distanceAwayTV.setText(marker.getSnippet());
		
		// Render schedule table programmatically
		// Source: http://stackoverflow.com/questions/1528988/create-tablelayout-programatically
		TableLayout.LayoutParams tableParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
		TableRow.LayoutParams rowParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);

		TableLayout tableLayout = new TableLayout(context);
		tableLayout.setLayoutParams(tableParams);

		TableRow tableRow = new TableRow(context);
		tableRow.setLayoutParams(tableParams);

		try {
			JSONArray raw = task.get();
			int size = raw.length();
			
			// If there is no schedule (i.e., no upcoming trains)
			if (size < 1) {
				TextView noScheduleTV = new TextView(context);
				noScheduleTV.setText(context.getString(R.string.no_schedule));
				popupView.addView(noScheduleTV);
			}
			else {
				// Table header rows
				TextView lineHeaderView = new TextView(context);
				lineHeaderView.setText(context.getString(R.string.schedule_table_header_1));
				lineHeaderView.setBackgroundResource(R.drawable.table_cell);
				lineHeaderView.setTypeface(null, Typeface.BOLD);
				lineHeaderView.setLayoutParams(rowParams);
				TextView directionHeaderView = new TextView(context);
				directionHeaderView.setText(context.getString(R.string.schedule_table_header_2));
				directionHeaderView.setBackgroundResource(R.drawable.table_cell);
				directionHeaderView.setTypeface(null, Typeface.BOLD);
				directionHeaderView.setLayoutParams(rowParams);
				TextView timeRemainingHeaderView = new TextView(context);
				timeRemainingHeaderView.setText(context.getString(R.string.schedule_table_header_3));
				timeRemainingHeaderView.setBackgroundResource(R.drawable.table_cell);
				timeRemainingHeaderView.setTypeface(null, Typeface.BOLD);
				timeRemainingHeaderView.setLayoutParams(rowParams);
				tableRow.addView(lineHeaderView);
				tableRow.addView(directionHeaderView);
				tableRow.addView(timeRemainingHeaderView);
				tableLayout.addView(tableRow);
				
				for (int count = 0; count < size; count++) {
					JSONObject entry = raw.getJSONObject(count);
					TextView lineTV = new TextView(context);
					lineTV.setBackgroundResource(R.drawable.table_cell);
					String line = entry.getString("line");
					if (line.compareTo("Red") == 0) {
						lineTV.setText(line);
						lineTV.setBackgroundColor(Color.RED);
						lineTV.setTextColor(Color.WHITE);
					}
					else if (line.compareTo("Orange") == 0) {
						lineTV.setText(line);
						lineTV.setBackgroundColor(Color.argb(255, 255, 165, 0));
						lineTV.setTextColor(Color.WHITE);
					}
					else if (line.compareTo("Blue") == 0) {
						lineTV.setText(line);
						lineTV.setBackgroundColor(Color.BLUE);
						lineTV.setTextColor(Color.WHITE);
					}
					else if (line.compareTo("CR") == 0) {
						lineTV.setText(entry.getString("trip"));
						lineTV.setBackgroundColor(Color.MAGENTA);
						lineTV.setTextColor(Color.WHITE);
					}
					lineTV.setLayoutParams(rowParams);
					TextView directionTV = new TextView(context);
					directionTV.setBackgroundResource(R.drawable.table_cell);
					directionTV.setText(entry.getString("direction"));
					directionTV.setLayoutParams(rowParams);
					TextView timeRemainingTV = new TextView(context);
					timeRemainingTV.setBackgroundResource(R.drawable.table_cell);
					timeRemainingTV.setText(entry.getString("time_remaining"));
					timeRemainingTV.setLayoutParams(rowParams);
					TableRow row = new TableRow(context);
					row.setLayoutParams(tableParams);
					row.addView(lineTV);
					row.addView(directionTV);
					row.addView(timeRemainingTV);
					tableLayout.addView(row);
				}
				// Remember to attach table to popup view
				popupView.addView(tableLayout);				
			}
		}
		catch (Exception e) {
			Crashlytics.logException(e);
		}

		return popupView;
	}
}
