package edu.tufts.cs.mchow.matransit;

import com.crashlytics.android.Crashlytics;

import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends Activity {
	private final int DEFAULT_ZOOM_LEVEL = 13;
	private final int DEFAULT_GPS_MIN_TIME = 120000; // in milliseconds
	private final int DEFAULT_GPS_MIN_DISTANCE = 1; // in meters
	private final int DEFAULT_MAP_ANIMATION_MILLIS = 2000;
	private LocationManager lm;
	protected static ProgressDialog progress;
	protected static GoogleMap map;
	protected static MyLocation me;
	protected static TextView mapFeedback;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		progress = new ProgressDialog(this);
		progress.setMessage(getString(R.string.progress_text));
		progress.show();
		setContentView(R.layout.activity_main);
		mapFeedback = (TextView)findViewById(R.id.mapFeedback);
		initMapAndLocation();
	}

	private void initMapAndLocation(){
		// Set up location detection stuff
		lm = (LocationManager) this.getSystemService(LOCATION_SERVICE);
		
		// Set up map
		// More info at http://www.vogella.com/articles/AndroidGoogleMaps/article.html
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

		// Set up customer map info window adapter
		map.setInfoWindowAdapter(new SchedulePopupWindowAdapter(this.getLayoutInflater(), this));

		// Set up my location marker with listener
		me = new MyLocation(this);
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,
						DEFAULT_GPS_MIN_TIME,
						DEFAULT_GPS_MIN_DISTANCE,
						me);	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_about:
				Toast.makeText(this, R.string.about_text, Toast.LENGTH_LONG).show();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public void redrawLocation() {
		LatLng pos;
		
		if (me.found) {
			map.clear();
			pos = new LatLng(me.myLat, me.myLng);
			map.addMarker(new MarkerOptions().position(pos).title(me.myLocStr));
			mapFeedback.setText("Your approximate location: " + me.myLocStr);
			
			// Get and display closest stations
			new GetClosestStations().execute(pos);
		}
		else {
			pos = new LatLng(42.359961,-71.056753);
			map.addMarker(new MarkerOptions().position(pos).title("Faneuiel Hall"));
			mapFeedback.setText(getString(R.string.unknown_location));
		}
		map.moveCamera(CameraUpdateFactory.newLatLng(pos));
		map.animateCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM_LEVEL), DEFAULT_MAP_ANIMATION_MILLIS, null);
	}
}
